DROP TABLE IF EXISTS linuxCommand;

CREATE TABLE linuxCommand (
    command VARCHAR(50) PRIMARY KEY,
    description VARCHAR(300) NOT NULL
);

INSERT INTO linuxCommand
VALUES ('cd', 'Change directory'),
    ('mkdir', 'Create a directory'),
    ('ls', 'list files in the current directory');