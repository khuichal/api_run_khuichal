FROM debian:stretch
RUN apt-get update && \
	apt-get install -y nginx && \
	rm -rf /var/lib/apt/lists/*
COPY config/kiki_site.conf /etc/nginx/conf.d/default.conf
# On placera les sites html ici
VOLUME [ "/usr/share/nginx/html" ]  
EXPOSE 80
# CMD + executable (comme execv)
CMD [ "/usr/sbin/nginx" ]
