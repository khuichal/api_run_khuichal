<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>10 commandes linux</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
    
    <h1>Les 10 commandes Linux</h1>    
<?php
function error_test($result_query){
    if ($result_query) {
        echo 'SQL query success';
    } else {
        echo 'Error with SQL Query';
    }
}

function display_table_command($resultset) {
    while ($row = $resultset->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr> <td>".$row['command']."</td><td>".$row['description']."</td> </tr>";
    }
}

$connexion = new PDO('pgsql:host=database_site2;port=5432;dbname=api', 'api', 'api');
if (!$connexion){
    echo "error during PDO connection";
}
$sql_query = 'SELECT * FROM linuxcommand ;'; 
$resultset = $connexion->prepare($sql_query);
$resultset->execute();
//error_test($result_query);

//affichage :
echo "<table>";
echo "  <tr>";
echo "      <th>";
echo "          Commande";
echo "      </th>";
echo "      <th>";
echo "          Description";
echo "      </th>";
echo "  </tr>";
display_table_command($resultset);
echo "</table>";


$connexion = null;
?>

</body>
</html>