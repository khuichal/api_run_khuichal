FROM debian:stretch
RUN apt-get update && \
	apt-get install -y nginx && \
	rm -rf /var/lib/apt/lists/*
COPY --chown=www-data:www-data api-run-app/ /var/www/html/
COPY config/web_myimage.conf /etc/nginx/nginx.conf
# On placera les sites html ici
VOLUME [ "/usr/share/nginx/html" ]  
EXPOSE 80
# CMD + executable (comme execv)
CMD [ "/usr/sbin/nginx" ]
