<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Table de multiplications</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
    <h1>Table de deux :</h1>
        <?php include "table_multi_2.php"; ?>

    <h1>Quelle table voulez-vous ? :</h1>
        <form method="post" action="table_multi.php" >
            <input type="number" name="num_table" id="num_table" max="10" min="0" placeholder="2">
            <input type="submit" value="Calculer !">
        </form>
</body>
</html>